var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var InterestSchema = new Schema({
  interestName: String,
  interestDescription: String
});

module.exports = mongoose.model('Interest', InterestSchema );