
  const interestObj = {
  surfing: {
    interestName: 'Surfing',
    interestImageSrc: 'https://dummyimage.com/100x100/000/fff',
    interestDescription: 'Cool breezing wind, riding on the blue ocean'
  },
  bowling: {
    interestName: 'Bowling',
    interestImageSrc:'https://dummyimage.com/100x100/000/fff',
    interestDescription: 'Try to bowl all skittles in the bowling alley'
  },
  rafting: {
    interestName: 'Rafting',
    interestImageSrc: 'https://dummyimage.com/100x100/000/fff',
    interestDescription: 'Enjoy the thrill in piercing through water'
  }
}

module.exports = interestObj;
