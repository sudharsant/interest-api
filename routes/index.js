var express = require('express');
var router = express.Router();

// var inter = require('./interestContent');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { 
    title: 'Interests',

    surfing: {
      interestName: 'Surfing',
      interestImageSrc: 'https://dummyimage.com/100x100/000/fff',
      interestDescription: 'Cool breezing wind, riding on the blue ocean'
    },
    bowling: {
      interestName: 'Bowling',
      interestImageSrc:'https://dummyimage.com/100x100/000/fff',
      interestDescription: 'Try to bowl all skittles in the bowling alley'
    },
    rafting: {
      interestName: 'Rafting',
      interestImageSrc: 'https://dummyimage.com/100x100/000/fff',
      interestDescription: 'Enjoy the thrill in piercing through water'
    },
    skiing: {
      interestName: 'Skiing',
      interestImageSrc: 'https://dummyimage.com/100x100/000/fff',
      interestDescription: 'Lush white scenary with high speed skiing'
    },
    bungee: {
      interestName: 'Bungee Jumping',
      interestImageSrc: 'https://dummyimage.com/100x100/000/fff',
      interestDescription: 'Adrenaline kick through your body and the excitement!!'
    },
    skydiving: {
      interestName: 'Sky Diving',
      interestImageSrc: 'https://dummyimage.com/100x100/000/fff',
      interestDescription: 'Just jump out the plane'
    },
    foodie: {
      interestName: 'Foodie',
      interestImageSrc: 'https://dummyimage.com/100x100/000/fff',
      interestDescription: 'Luxurious and lavish taste of different continental cuisine'
    },
    fishing: {
      interestName: 'Fishing',
      interestImageSrc: 'https://dummyimage.com/100x100/000/fff',
      interestDescription: 'Catch as many as you like'
    },
    safari: {
      interestName: 'Safari',
      interestImageSrc: 'https://dummyimage.com/100x100/000/fff',
      interestDescription: 'Witness the true beaty of nature'
    }
  });
});

module.exports = router;
