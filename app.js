var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var users = require('./routes/users');

var app = express();
var router = express.Router();

var port = process.env.PORT || 8000; 

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test');

var Interest = require('./src/models/interestSchema');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req,res) {
  console.log("heys");
  res.end('Welcome to homepage go to user for selecting Interests.');
});

app.use('/user', router);

router.route('/interest')
//create interests
.post(function(req,res) {
  var interest = new Interest();
  interest.interestName = req.body.interestName;
  interest.interestDescription = req.body.interestDescription;
  console.log(req.body._id);

  //save and check for errors
  interest.save(function(err) {
    if(err) 
      res.send(err);

    res.send("Interest Saved");
  })
})
//get all interests
.get(function(req,res) {
  Interest.find(function(err,interest) {
    if(err)
      res.send(err);

    res.json(interest);
  })
})

router.route('/interest/:interest_id')
//get bear by id
.get(function(req,res) {
  Interest.findById(req.params.interest_id, function(err, interest) {
    if(err)
      res.send(err)

    res.json(interest);    
  })
})

.put(function(req, res) {
  Interest.findById(req.params.interest_id, function(err, interest) {

    if (err)
      res.send(err);

    interest.interestName = req.body.interestName;
    interest.save(function(err) {
      if (err)
        res.send(err);

      res.send('Interest updated!');
    });
  });
})

.delete(function(req,res) {
  Interest.remove({
    _id: req.params.interest_id
  }, function(err, interest) {
      if(err)
        res.send(err)

      res.send('Interests Deleted')
  });
});

app.use('/user',index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(port);

console.log('The port used is:'+port);

module.exports = app;
